{ pkgs, ... }:

{
  users.extraUsers.leo60228 = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keyFiles = [ /home/leo60228/.ssh/id_rsa.pub ];
  };
  
  users.users.root.openssh.authorizedKeys.keyFiles = [ /home/leo60228/.ssh/id_rsa.pub ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
  
  nixpkgs.config.allowUnfree = true;

  # trusted users
  nix.trustedUsers = [ "root" "@wheel" ];
  
  environment.systemPackages = with pkgs; [
    openssh dysnomia disnix
  ];

  services.openssh.enable = true;
  services.disnix.enable = true;

  networking.interfaces.lo.ipv4.addresses = [ { address = "127.0.0.1"; prefixLength = 32; } { address = "127.225.0.0"; prefixLength = 16; } ];
  networking.localCommands = ''
! ${pkgs.iproute}/bin/ip addr del 127.0.0.0/8 dev lo
#DATEBIN="$(printf '%064s' "$(${pkgs.bc}/bin/bc <<< "obase=2;$(date '+%s')")" | tr ' ' '0')"
#NAMEBIN="$(${pkgs.bc}/bin/bc <<< "obase=2;ibase=16;1E060228")"
#REPETITIONS="$(((64+''${#NAMEBIN}-1) / ''${#NAMEBIN}))"
#REPEATEDNAMEBIN="$(yes "$NAMEBIN" | head -n 64 | tr -d '\n')"
#CUTNAMEBIN="$(echo $REPEATEDNAMEBIN | cut -c -64)"
#KEY="''${DATEBIN}''${CUTNAMEBIN}"
#HEXSHA="$(echo $KEY | ${pkgs.perl}/bin/shasum -0 | cut -f1 '-d ' | tr 'a-z' 'A-Z')"
#SHA="$(BC_LINE_LENGTH=0 ${pkgs.bc}/bin/bc <<< "obase=2;ibase=16;$HEXSHA")"
#TRUNCSHA="''${SHA: -31}"
#IDENT="''${NAMEBIN:0:9}"
#GLOBALID="''${IDENT}''${TRUNCSHA}1"
#echo $GLOBALID
#BINTRUNCIP="1111110000000000$GLOBALID"
#BINIP="$(printf '%-128.128s' "$BINTRUNCIP" | tr ' ' '0')"
#HEXIP="$(${pkgs.bc}/bin/bc <<< "ibase=2;obase=10000;$BINIP")"
#${pkgs.iproute}/bin/ip -6 addr add "$(echo $HEXIP | ${pkgs.gnused}/bin/sed -r 's/(.{4})/\1:/g' | ${pkgs.gnused}/bin/sed 's/:$//')/64" dev lo scope host
${pkgs.iproute}/bin/ip -6 route add local "fd1e:0602:28:edef:0::/80" dev lo
  '';

  boot.tmpOnTmpfs = true;
}
