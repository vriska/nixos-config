{config, pkgs, lib, ...}:

let
  cfg = config.components;
in

with lib;

{
  options = {
    components._isComponent = mkOption {
      default = true;
      type = types.bool;
    };

    components.__functor = mkOption {
      default = null;
    };

    components._lastComponent = mkOption {
      default = "";
      type = types.string;
    };

    components._name = mkOption {
      default = "";
      type = types.string;
    };
  };

  imports =
    (map 
      (x: ../components + "/${x}.nix")
      (builtins.filter
        (x: x != "default")
        (map 
          (x: builtins.elemAt (builtins.match "(.*)\.nix" x) 0)
          (builtins.attrNames (builtins.readDir ../components))
        )
      )
    );
}
