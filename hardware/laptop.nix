{
  nixos = { config, lib, pkgs, ... }:
  {
    imports =
      [ <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
      ];

    boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
    boot.kernelModules = [ "kvm-intel" ];
    boot.extraModulePackages = [ ];

    fileSystems."/" =
    { device = "/dev/disk/by-uuid/67e33deb-3f7c-4de9-b3e4-023a961fb5e6";
      fsType = "ext4";
    };

    fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/FE84-ACB0";
      fsType = "vfat";
    };

    swapDevices = [ ];

    nix.maxJobs = lib.mkDefault 8;
    powerManagement.cpuFreqGovernor = "powersave";
  };

  nixops = {
    deployment.targetHost = "leotop.local";
  };
}
