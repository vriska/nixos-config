let lib = import ../lib; in
lib.makeComponent "vfio"
({cfg, pkgs, lib, ...}: with lib; {
  opts = {};

  config = {
    boot.kernelModules = [ "kvm-amd" "kvm-intel" ];
    
    boot.kernelParams = [ "intel_iommu=on,igfx_off" "kvm.ignore_msrs=1" ];
    boot.initrd.kernelModules = [ "vfio" "vfio_iommu_type1" "vfio_pci" "vfio_virqfd" "vhost-net" ];

    # virtualbox
    virtualisation.virtualbox.host.enable = true;
  };
})
