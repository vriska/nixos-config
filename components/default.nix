let lib = import ../lib; in builtins.listToAttrs
  (map 
    (x: {
      name = x;
      value = lib.componentize "../components/${x}.nix";
    })
    (builtins.filter
      (x: x != "default")
      (map 
        (x: builtins.elemAt (builtins.match "(.*)\.nix" x) 0)
        (builtins.attrNames (builtins.readDir ./.))
      )
    )
  )
