let lib = import ../lib; in
lib.makeComponent "kde"
({cfg, pkgs, lib, ...}: with lib; {
  opts = {
    bluetooth = mkOption {
      default = false;
      type = types.bool;
    };
  };

  config = {
    environment.systemPackages = with pkgs; [
      plasma-nm plasma-pa bluedevil
    ];

    # Enable the KDE Desktop Environment.
    services.xserver.displayManager.sddm.enable = true;
    services.xserver.desktopManager.plasma5.enable = true;
  };
})
