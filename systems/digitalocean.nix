{ pkgs, ... }: with import ../components; rec {
  components = en_us est docker extra cloud9 shellinabox;

  boot.cleanTmpDir = true;
  networking.hostName = "digitaleo";

  networking.firewall.allowedTCPPorts = [ 22 80 3000 5901 5900 6080 5901 4333 21 ];

  # will remove once i eventually write service support into my config
  systemd.services.smsforwarder.description = "Discord.js SMS forwarder bot";
  systemd.services.smsforwarder.script = "${pkgs.nodejs-10_x}/bin/node /home/leo60228/discordsms/index.js";
  systemd.services.smsforwarder.serviceConfig.Restart = "always";
  systemd.services.smsforwarder.serviceConfig.RestartSec = 10;
  systemd.services.smsforwarder.wantedBy = [ "multi-user.target" ];

  systemd.services.rclone.script = "${pkgs.sudo}/bin/sudo -i -u leo60228 -- ${pkgs.rclone}/bin/rclone mount gdrive:everest /home/everestreleases/";
  systemd.services.rclone.wantedBy = [ "multi-user.target" ];

  users.users.everestreleases = {
    isSystemUser = true;
    home = "/home/everestreleases";
  };

  services.vsftpd.enable = true;
  services.vsftpd.anonymousUser = true;
  services.vsftpd.anonymousUserHome = "/home/everestreleases";
  services.vsftpd.anonymousUserNoPassword = true;
  services.vsftpd.chrootlocalUser = true;
  services.vsftpd.userlistEnable = true;
  services.vsftpd.userlistFile = pkgs.writeText "userlist" "everestreleases";
  services.vsftpd.writeEnable = true;
  services.vsftpd.localUsers = true;

  security.sudo.extraConfig = ''
Defaults runaspw
  '';
}
